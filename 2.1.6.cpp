#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

void gotoxy(int x,int y)         //将光标移动到（x,y）位置
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE)
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
}

void show()                     //显示屏幕 
{
	system("cls");              //清屏 
	int i,j;
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if((i==position_x)&&(j==position_y))
			   printf("*");     //输出飞机* 
			else if((i==enemy_x)&&(j==enemy_y))
			   printf("@");     //输出敌机@ 
			else if((i==bullet_x)&&(j==bullet_y))
			   printf("|");     //输出子弹| 
			else
			   printf(" ");     //输出空格 
		}
		printf("\n");
	}
        printf("得分: %d\n",score);
}

void  HideCursor()
{
    CONSOLE_CURSOR_INFO cursor_info={1,0};  //第二个值为 0 表示隐藏光标
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info)	
}
int main()
{
	HideCursor();               //隐藏光标
	return 0;
}
