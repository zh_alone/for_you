#include<graphics.h>
#include<conio.h>
#include<math.h>

#define High 480
#define Width 640

int main()
{
	initgraph(Width,High);
	int center_x,center_y;
	center_x = Width/2;
	center_y = High/2;
	int secondLength;
	secondLength = Width/5;
	
	int secondEnd_x,secondEnd_y;
	
	secondEnd_x = center_x + secondLength;
	secondEnd_y = center_y;
	
	//画秒针
	setlinestyle(PS_SOLID,2);
	setcolor(WHITE);
	line(center_x,center_y,secondEnd_x,secondEnd_y); 
	getch();
	closegraph();
	return 0;
	
}

//要用 Easyx 
#include<graphics.h>
#include<conio.h>
#include<math.h>

#define High 480
#define Width 640
#definePI 3.14159

int main()
{
	initgraph(Width,High);         //初始化 640*480 的绘图窗口 
	int center_x,center_y;         //中心点的坐标，也是钟表的中心 
	center_x = Width/2;
	center_y = High/2;
	int secondLength;              //秒针的长度 
	secondLength = Width/5;
	
	int secondEnd_x,secondEnd_y;   //秒针的终点 
	float secondAngle = 0;         //秒针对应的长度 
	
	secondEnd_x = center_x + secondLength;
	secondEnd_y = center_y;        
	
	while(1)
	{
	  	//由角度决定的秒针终点坐标
		secondEnd_x = center_x + secondLength * sin(secondAngle);
	    secondEnd_y = center_y - secondLength * cos(secondAngle);
	                                
	    setlinestyle(PS_SOLID,2);       //画实线，宽度为两个像素 
	    setcolor(WHITE);
     	line(center_x,center_y,secondEnd_x,secondEnd_y);     //画秒针
     	
     	sleep(100);
     	
     	setcolor(BLACK);
     	line(center_x,center_y,secondEnd_x,secondEnd_y);     //一圈一共 2*PI，一圈 60 秒，一秒钟秒针走
		                                                     //过的角度为 2*PI/60 
    }
	getch();                        //按任意键继续
	closegraph();                   //关闭绘图窗口 
	return 0;      
	
	
}
	//要用 Easyx 
