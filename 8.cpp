#include<stdio.h>

int main()
{
	int i,n,*p;
	double max,min,total=0;
	scanf("%d",&n);
	if((p=(int *)calloc(n,sizeof(int)))==NULL){
		printf("Not able to allocate memory. \n");
		exit(1);
	}
    for(i=0;i<n;i++) 
	{
		scanf("%d",p+i);
		total+=*(p+i);
	}
	max=min=*p;
	for(i=0;i<n;i++)
	{
		if(max<=*(p+i)){
			max=*(p+i);
		}
		if(min>=*(p+i)){
			min=*(p+i);
		}
	}
	printf("average = %.2f\nmax = %.2f\nmin = %.2f",total/n,max,min);
	free(p);
	return 0;
	
}
