#include <stdio.h>
#include <stdlib.h>
int main()
{
    int n,i,*p;
    double max,min,total=0;
    scanf("%d",&n);
    if((p=(int*)calloc(n,sizeof(int)))==NULL){
        printf("Not able to allocate memory. \n");
        exit (1);
    }
    for(i=0;i<n;i++){
        scanf("%d",p+i);
    }
    for(i=0;i<n;i++){
        total=total+*(p+i);
    }
    max=*p;
    for(i=1;i<n;i++){
        if(max<*(p+i))
            max=*(p+i);
    }
    min=*p;
    for(i=1;i<n;i++){
        if(min>*(p+i))
            min=*(p+i);
    }
     printf("average = %.2f\n",total/n);
     printf("max = %.2f\n",max);
     printf("min = %.2f\n",min);
     free(p);
     
    return 0;
}
